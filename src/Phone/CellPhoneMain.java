/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Phone;

public class CellPhoneMain {
    public static void main(String[] args) {
        CellPhone cp = new CellPhone("iPhone", "XS Max");
        cp.powerOn();
        cp.volumeUp();
        cp.volumeUp();
        cp.volumeDown();
        cp.powerOff();
        cp.powerOff();
        cp.topUpPulsa(50000);
        int sisaPulsa = cp.getSisaPulsa();
        System.out.println("Sisa pulsa: " + sisaPulsa);
        cp.tambahKontak("Oktisya Ruth Dianita", "081328524423");
        cp.tambahKontak("Yedida Farel Terang Immanuel", "083843304201");
        cp.lihatSemuaKontak();
        cp.cariKontak("Calvin Revianto");
        cp.cariKontak("Alatas Mufti Falah");
    }
}
